#include <iostream>
#include <string>
#include <map>
#include <set>
#include <functional>


char* fopen_read(const std::string& fileName, long& bufferSize){
    FILE* file = fopen(fileName.c_str(), "rb");
    if (!file){
        std::cout << "Input file not exist: " << fileName << std::endl;
        return nullptr;
    }        

    fseek(file, 0, SEEK_END);
    bufferSize = ftell(file);
    rewind(file);

    char* buffer = new char[bufferSize];
    fread(buffer, bufferSize, 1, file);
    fclose(file);
    return buffer;
}

void fopen_write(const std::string& fileName, std::map<int, std::set<std::string>, std::greater<int>>& freq_map){
    FILE* file = fopen(fileName.c_str(), "w");
    for (auto& n : freq_map){
        auto freq = std::to_string(n.first);
        for (auto& m : n.second){
            std::string str = freq + " " + m + "\n";
            fputs(str.c_str(), file);
        }
    }
    fclose(file);
}

void fill_frequency(char* buffer, const long& bufferSize, std::unordered_map<std::string, int>& map){    
    long start = -1, i = 0;
    while(i < bufferSize || start >= 0){
        if (buffer[i] >= 'A' && buffer[i] <= 'Z')
            buffer[i] += 32;
        const bool alpha = buffer[i] >= 'a' && buffer[i] <= 'z';
        if (alpha && start < 0)
            start = i;
        else if ((!alpha || i >= bufferSize) && start >= 0) {
            std::string key(buffer + start, i - start);
            map[key] += 1;
            start = -1;
        }          
        ++i;
    }
}

void frequency(const std::string& in, const std::string& out){
    long bufferSize = 0;
    char* buffer = fopen_read(in, bufferSize);

    std::unordered_map<std::string, int> map;
    fill_frequency(buffer, bufferSize, map);
    delete[] buffer;

    std::map<int, std::set<std::string>, std::greater<int>> freq_map;
    for (auto& n : map)
        freq_map[n.second].emplace(n.first);

    fopen_write(out, freq_map); 
}

int main(int argc, char* argv[]){
    if (argc < 3) {
        std::cout << "Usage: " << argv[0] << " [input_file] [output_file]" << std::endl;
        return 1;
    }

    std::string in{argv[1]};
    std::string out{argv[2]};

    frequency(in, out);   
  
    return 0;
}