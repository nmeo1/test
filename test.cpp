#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <utility>

struct ParamData{
    int index;
    int paramCount;
    std::string description;
};

class Params{
public:
    Params();
    bool Parse(int argc, char* argv[]);
    void AddUsage(std::string usage);
    void AddParameter(const std::string& flag, int count, const std::string& description);
    bool HasValue(const std::string& flag);
    std::vector<std::string> GetValue(const std::string& flag);
    void PrintHelp();
private:
    std::vector<std::string> _usage;
    std::map<std::string, ParamData > _parameters;
    std::unordered_map<int, std::vector<std::string>> _values;
    int _index = 0;
};

Params::Params() {
}

bool Params::Parse(int argc, char* argv[]){
    int index = -1, count = 0;
    bool success = true;
    for (int i = 1; i < argc; ++i) {
        if (index >= 0 && count > 0){
            _values[index].emplace_back(argv[i]);
            --count;
        }
        else if (_parameters.count(argv[i]) > 0){
            index = _parameters[argv[i]].index;
            count = _parameters[argv[i]].paramCount;
            if (count == 0)
                _values[index] = {};
        }           
        else {
            std::cout << "Unrecognized flag: " << argv[i] << std::endl;
            success = false;
        } 
    }
    if (count > 0) {
        std::cout << "Failed parse parameters!" << std::endl;
        success = false;
    }
        
    return success;
}

void Params::AddUsage(std::string usage){
    _usage.push_back(std::move(usage));
}

void Params::AddParameter(const std::string& flag, int count, const std::string& description){
    _parameters[flag] = {_index++, count, description};
}

bool Params::HasValue(const std::string& flag){
    if (_parameters.count(flag) > 0)
        if (_values.count(_parameters[flag].index) > 0)
            return true;
    return false;
}

std::vector<std::string> Params::GetValue(const std::string& flag){
    if (HasValue(flag))
        return _values[_parameters[flag].index];

    return {};
}

void Params::PrintHelp(){
    if (!_usage.empty()){
        std::cout << "Usage:" << std::endl;
        for (auto& usageLine : _usage)
            std::cout << " " << usageLine << std::endl;
        std::cout << std::endl;
    }

    std::cout << "Flags:" << std::endl;
    for (auto& param : _parameters)
        std::cout << " " << param.first << " " << param.second.description << std::endl;
}

bool ReadFile(const std::string& fileName, char** buffer, size_t& fileSize){
    std::ifstream fstream(fileName, std::ios::binary | std::ios::in | std::ios::ate);
    if (fstream.is_open()){
        fileSize = static_cast<size_t>(fstream.tellg());
        *buffer = new char[fileSize];    
        fstream.seekg(0, std::ios::beg);
        fstream.read(*buffer, fileSize);
        fstream.close();
    }
    else {
        std::cout << "Input file not exist: " << fileName << std::endl;
        return false;
    }
    return true;
}

int CountSubstrings(const std::string& fileName, const std::string& subString){
    size_t subSize = subString.size();
    if (subSize == 0)
        return 0;

    int subCount = 0;
    char* buffer = nullptr;
    size_t fileSize = 0;
    if (!ReadFile(fileName, &buffer, fileSize))
        return 0;

    size_t subIndex = 0, continueIndex = 0;
    for (size_t i = 0; i < fileSize; ++i){
        if (buffer[i] == subString[subIndex]){
            if (subIndex == 0)
                continueIndex = i;
            ++subIndex;
        }
        else if (subIndex > 0){
            i = continueIndex;
            subIndex = 0;
        }
        else
            subIndex = 0;

        if (subIndex >= subSize){
            ++subCount;
            subIndex = 0;
            i = continueIndex;
        }
    }

    delete[] buffer;
    return subCount;
}


unsigned int GetCheckSum(const std::string& fileName){
    unsigned int sum = 0;

    char* buffer = nullptr;
    size_t fileSize = 0;
    if (!ReadFile(fileName, &buffer, fileSize)){
        return 0;
    }

    // Для файла с текстовым представлением 01234567ABCD чексумма должна получиться 2779294638
    unsigned int num, byte1, byte2, byte3, byte4;
    for (size_t i = 0; i < fileSize; i += sizeof(sum)){
        byte1 = buffer[i] << 24;
        byte2 = i + 1 < fileSize ? buffer[i + 1] << 16 : 0;
        byte3 = i + 2 < fileSize ? buffer[i + 2] << 8 : 0;
        byte4 = i + 3 < fileSize ? buffer[i + 3] : 0;
        num = byte1 | byte2 | byte3 | byte4;
        sum += num;
    }

    delete[] buffer;
    return sum;
}


int main(int argc, char* argv[]){
    const std::string fFile = "-f";
    const std::string fCommand = "-m";
    const std::string fSubstring = "-v";
    const std::string fHelp = "-h";
    const std::string commandWords = "words";
    const std::string commandSum = "checksum";
    const std::string execName = argv[0];

    Params params;
    params.AddUsage(execName + " -f Test.tst -m words -v mother — печатает количество слов «mother» в файле «Test.tst»");
    params.AddUsage(execName + " -f Test.tst -m checksum — печатает 32-битную чексумму, рассчитанную по алгоритму checksum = word1 + word2 + ... + wordN (word1..wordN – 32-хбитные слова, представляющие содержимое файла)");
    params.AddUsage(execName + " -h — печатает информацию о программе и описание параметров.");
    params.AddParameter(fFile, 1, "[input_file] - входящий файл");
    params.AddParameter(fCommand, 1, "[command] - выбрать команду, доступные: " + commandWords + ", " + commandSum);
    params.AddParameter(fSubstring, 1, "[substring] - подстрока для поиска");
    params.AddParameter(fHelp, 0, "- выводит справку");

    if (!params.Parse(argc, argv)){
        std::cout << std::endl;
        params.PrintHelp();
        return 1;
    }

    if (params.HasValue(fCommand) && params.HasValue(fFile)){
        const std::string fileName = params.GetValue(fFile)[0];
        const std::string command = params.GetValue(fCommand)[0];
        if (command == commandSum)
            std::cout << GetCheckSum(fileName) << std::endl;
        else if (command == commandWords && params.HasValue(fSubstring))
            std::cout << CountSubstrings(fileName, params.GetValue(fSubstring)[0]) << std::endl;       
    }
    else if (params.HasValue(fHelp))
        params.PrintHelp();

    return 0;
}